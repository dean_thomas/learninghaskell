# Prerequisities
sudo apt-get update
sudo apt-get install -y python3-pip git libtinfo-dev libzmq3-dev libcairo2-dev libpango1.0-dev libmagic-dev libblas-dev liblapack-dev jupyter

# IHaskell installation
curl -sSL https://get.haskellstack.org/ | sh
git clone https://github.com/gibiansky/IHaskell
cd IHaskell
pip3 install -r requirements.txt
stack install gtk2hs-buildtools
stack install --fast

# make sure ihaskell can be found on PATH
sh -c "cd /usr/local/bin && sudo ln -sf ~/.local/bin/ihaskell ihaskell"

ihaskell install --stack
