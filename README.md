# README #

This project contains the code generated whilst working through the examples in the book 'Learn you a Haskell for great good'.

See: http://learnyouahaskell.com for the book tutorials [12-04-2016].
See: https://github.com/gibiansky/IHaskell for IHaskell installation instructions [09-09-2018] .

##	To setup ##

Install IHaskell for Jupyter using the setup_ihaskell shell script.

## To run ##

Run the jupyter_server shell script.