
lucky :: Int -> String
lucky 7 = "LUCKY NUMBER 7!"
lucky x = "Sorry, you're out of luck pal!"

lucky 5

lucky 7

sayMe :: Int -> String
sayMe 1 = "One!"
sayMe 2 = "Two!"
sayMe 3 = "Three!"
sayMe 4 = "Four!"
sayMe 5 = "Five!"
sayMe x = "Not between 1 and 5"

sayMe 4

sayMe 2

sayMe 81

sayMe' :: Int -> String
sayMe' x = "Not between 1 and 5"
sayMe' 1 = "One!"
sayMe' 2 = "Two!"
sayMe' 3 = "Three!"
sayMe' 4 = "Four!"
sayMe' 5 = "Five!"

sayMe' 4

sayMe 2

sayMe 81

factorial :: Integer -> Integer
factorial 0 = 1
factorial n = n * factorial(n - 1)

factorial 3

factorial 100

factorial 0

charName :: Char -> String
charName 'a' = "Albert"
charName 'b' = "Broseph"
charName 'c' = "Cecil"

charName 'a'

charName 'b'

charName 'h'

addVectors :: (Double , Double) -> (Double, Double) -> (Double, Double)
addVectors a b = (fst a + fst b, snd a + snd b)

addVectors (2, 3) (4, 5)

addVectors' :: (Double , Double) -> (Double, Double) -> (Double, Double)
addVectors' (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

addVectors' (2, 3) (4, 5)

first :: (a, b, c) -> a
first (x, _, _) = x

second :: (a, b, c) -> b
second (_, y, _) = y

third :: (a, b, c) -> c
third (_, _, z) = z

first (1, 2, 3)

second (2, 4, 6)

third (3, 6, 9)

let xs = [(1, 3), (4, 3), (2, 4), (5, 3), (5, 6), (3, 1)]

[a + b | (a, b) <- xs]

head' :: [a] -> a
head' [] = error "Can't call head on an empty list, dummy!"
head' (x:_)= x

head' [4, 5, 6]

head' "Hello"

:t error

tell :: (Show a) => [a] -> String
tell [] = "The list is empty"
tell (x:[]) = "The list has one element: " ++ show x
tell (x:y:[]) = "The list has two elements: " ++ show x ++ " and " ++ show y
tell (x:y:_) = "The list is long.  The first two elements are: " ++ show x ++ " and " ++ show y

tell []

tell [1]

tell [3, 6, 7, 79]

length' :: (Num b) => [a] -> b
length' [] = 0
length' (_:xs) = 1 + length' xs

length' []

length' [44, 56, 77, 103]

length' "Ham"

sum' :: (Num a) => [a] -> a
sum' [] = 0
sum' (x:xs) = x + sum' xs

sum' [3, 5]

sum' [1, 2, 3, 4, 5, 6]

capital :: String -> String
capital "" = "Empty string, whoops!"
capital all@(x:xs) = "The first letter of " ++ all ++ " is " ++ [x]

capital "Dracula"

bmiTell :: (RealFloat a) => a -> String
bmiTell bmi
    | bmi <= 18.5 = "You're underweight, you emo, you!"
    | bmi <= 25.0 = "You're supposedly normal.  Pffft, I bet you're ugly!"
    | bmi <= 30.0 = "You're fat!  Lose some weight, fatty!"
    | otherwise   = "You're a whale, congratulations!"

bmiTell 20.4

bmiTell 42.5

bmiTell :: (RealFloat a) => a -> a -> String
bmiTell weight height
    | weight / height ^ 2 <= 18.5 = "You're underweight, you emo, you!"
    | weight / height ^ 2 <= 25.0 = "You're supposedly normal.  Pffft, I bet you're ugly!"
    | weight / height ^ 2 <= 30.0 = "You're fat!  Lose some weight, fatty!"
    | otherwise                    = "You're a whale, congratulations!"

bmiTell 90 1.50

bmiTell 75 1.86

bmiTell 22.1 0.75

max' :: (Ord a) => a -> a -> a
max' a b
    | a > b     = a
    | otherwise = b

max 3 5

max 6 6

max' :: (Ord a) => a -> a -> a
max' a b| a > b = a | otherwise = b

max 3 5

myCompare :: (Ord a) => a -> a -> Ordering
a `myCompare` b
    | a > b     = GT
    | a == b    = EQ
    | otherwise = LT

3 `myCompare` 5

bmiTell :: (RealFloat a) => a -> a -> String
bmiTell weight height
    | bmi <= 18.5 = "You're underweight, you emo, you!"
    | bmi <= 25.0 = "You're supposedly normal.  Pffft, I bet you're ugly!"
    | bmi <= 30.0 = "You're fat!  Lose some weight, fatty!"
    | otherwise = "You're a whale, congratulations!"
    where bmi = weight / height ^ 2

bmiTell 67 1.33

bmiTell :: (RealFloat a) => a -> a -> String
bmiTell weight height
    | bmi <= skinny = "You're underweight, you emo, you!"
    | bmi <= normal = "You're supposedly normal.  Pffft, I bet you're ugly!"
    | bmi <= fat = "You're fat!  Lose some weight, fatty!"
    | otherwise = "You're a whale, congratulations!"
    where bmi = weight / height ^ 2
          skinny = 18.5
          normal = 25.0
          fat = 30.0

bmiTell 100 1.7

bmiTell 72 1.7

bmiTell :: (RealFloat a) => a -> a -> String
bmiTell weight height
    | bmi <= skinny = "You're underweight, you emo, you!"
    | bmi <= normal = "You're supposedly normal.  Pffft, I bet you're ugly!"
    | bmi <= fat = "You're fat!  Lose some weight, fatty!"
    | otherwise = "You're a whale, congratulations!"
    where bmi = weight / height ^ 2
          (skinny, normal, fat) = (18.5, 25.0, 30.0)

initials :: String -> String -> String
initials firstname lastname = [f] ++ ". " ++ [l] ++ "."
    where (f:_) = firstname
          (l:_) = lastname

initials "Dean" "Thomas"

calcBmis :: (RealFloat a) => [(a, a)] -> [a]
calcBmis xs = [bmi w h | (w, h) <- xs]
    where bmi weight height = weight / height ^ 2

calcBmis [(75.3, 1.3), (90.2, 1.58), (100.3, 1.94)]

cylinder :: (RealFloat a) => a -> a -> a
cylinder r h =
    let sideArea = 2 * pi * r * h
        topArea = pi * r ^ 2
    in sideArea + 2 * topArea

cylinder 5.3 10.0

[let square x = x * x in (square 5, square 3, square 2)]

(let a = 100; b = 200; c = 300 in a*b*c, let foo="Hey "; bar = "there!" in foo ++ bar)

(let (a, b, c) = (1, 2, 3) in a + b + c) * 100

calcBmis' :: (RealFloat a) => [(a, a)] -> [a]
calcBmis' xs = [bmi | (w, h) <- xs, let bmi = w /h ^ 2]

calcBmis' [(75.3, 1.3), (90.2, 1.58), (100.3, 1.94)]

let zoot x y z = x * y + z

zoot 3 9 2

let boot x y z = x * y + z in boot 3 4 2

boot 3 4 2

head' :: [a] -> a
head' [] = error "No head for empty lists!"
head' (x:_) = x

head'' :: [a] -> a
head'' xs = case xs of [] -> error "No head for expty lists!"
                       (x:_) -> x 

head' [3, 4, 5, 6]

head'' [3, 4, 5, 6]

describeList :: [a] -> String
describeList xs = "The list is " ++ case xs of [] -> "empty."
                                               [x] -> "a singleton list."
                                               xs -> "a longer list."

describeList []

describeList [2]

describeList [[2, 4], [3, 5]]

describeList' :: [a] -> String
describeList' xs = "The list is " ++ what xs
    where what [] = "empty."
          what [x] = "a singleton list."
          what xs = "a longer list."

describeList' []

describeList' [2]

describeList' [[2, 4], [3, 5]]


