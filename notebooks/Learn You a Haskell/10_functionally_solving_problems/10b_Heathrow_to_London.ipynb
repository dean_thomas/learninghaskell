{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Heathrow to London\n",
    "This next example is a program that represents a road system between London and Heathrow.  The program should be able to print out the shortest task between the two locations.  The layout is represented as a series of numbers; road A and road B run parallel to the destination, at defined intervals ($n_a$, $n_b$) they are crossed by other roads with length ($n_c$).\n",
    "\n",
    "In order to solve this problem we take a similar approach to the RPN calculator.  First, the problem is solved by hand.  We then think about how it can be represented in Haskell code.  Finally, the code is written in Haskell and the solution can be checked.\n",
    "\n",
    "To solve the problem, in summary, we first calculate the best path to the next crossroads.  We then have the option to continue ahead or cross to the other road.  At each step we compute the cheapest path and remember it for the next step.  This is repeated until the destination is reached.  In essence we keep track of the shortest path on road A and the shortest path on road B.  The shorter of these two will be the overall solution.\n",
    "\n",
    "Next we must workout which of Haskells data types are suitable for representing the problem.  We can consider the starting points and roads as a graph structure.  All crossroad point to another node, apart from the final destination.  We can therefore code the data types as follows initially:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "data Node = Node Road Road | EndNode Road\n",
    "data Road = Road Int Node"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nodes either contain information about the road that leads to the other mainroad and the next node on the current road, or the end node.  Each road has information about how long it is and what node it points to.  As an example. the first section of road A would be represented as *Road 50 a1* where the road is *50* units long and points to crossroads *a1*.  The node *a1* would be represented as *Node x y* where *x* and *y* are the roads pointing to *B1* and *A2*.\n",
    "\n",
    "Alternatively, we could use **Maybe** for the parts of the road that point forward.  End points wouldn't have an additional road."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "data Node = Node Road (Maybe Road)\n",
    "data Road = Road Int Node"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Whilst this is a suitable method for representing the road system in Haskell, there are simpler ways.  By thinking about how to solve the problem on paper we can think of the problem as always being a check of 3 road lengths at each stage.  These are the road section on road A, the road section on road B, and the road linking A and B.  Therefore we could simplify the data types further:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "data Section = Section { getA :: Int, getB :: Int, getC :: Int } deriving (Show)\n",
    "type RoadSystem = [Section]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "It is always good to keep data types as simple as possible.  *Section* provides a simple algebraic data type representing the length of 3 roads, *RoadSection* is a type synonym used to represent the system as a whole.\n",
    "\n",
    "Whilst it would be possible to use a tuple to represent the 3 roads, it is often better to create a custom algebraic data type.  This means it is possible to distinguish between different types formed of the same parts.  For example a Vector type and a Section type may both be represented by 3 integers but their use shouldn't be confused.\n",
    "\n",
    "Using our new data types we can represent the Heathrow to London road system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "heathrowToLondon :: RoadSystem\n",
    "heathrowToLondon = [\n",
    "    Section 50 10 30, \n",
    "    Section 5 90 20, \n",
    "    Section 40 2 25,\n",
    "    Section 10 8 0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we need to implement the solution as Haskell code.  Our function should take a *RoadSystem* parameter and return a *Path*.  We'll define a *Label* enumeration and the *Path* type synonym below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "data Label = A | B | C deriving (Show)\n",
    "type Path = [(Label, Int)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function will be called *optimalPath*.  The path will be accumulated as the list of sections is evaluated; hence, we will used a left fold.\n",
    "\n",
    "In the hand written solution the step to check the optimal paths on A and B so far is repeated multiple times.  The type of the function used to evaluate this is of the form:\n",
    "```Haskell\n",
    "(Path, Path) -> Section -> (Path, Path)\n",
    "```\n",
    "This can be used as the binary function for the left fold used in the accumulator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "roadStep :: (Path, Path) -> Section -> (Path, Path)\n",
    "roadStep (pathA, pathB) (Section a b c) =\n",
    "    let priceA = sum $ map snd pathA\n",
    "        priceB = sum $ map snd pathB\n",
    "        forwardPriceToA = priceA + a\n",
    "        crossPriceToA = priceB + b + c\n",
    "        forwardPriceToB = priceB + b\n",
    "        crossPriceToB = priceA + a + c\n",
    "        newPathToA = if forwardPriceToA <= crossPriceToA\n",
    "                        then (A, a):pathA\n",
    "                        else (C, c):(B, b):pathB\n",
    "        newPathToB = if forwardPriceToB <= crossPriceToB\n",
    "                        then (B, b):pathB\n",
    "                        else (C, c):(A, a):pathA\n",
    "    in (newPathToA, newPathToB)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "We calculate the optimal price on road A based upon the existing best route on A.  The same process is repeated for B.  We then compute the price sum for continuing straight ahead on A, and for crossing to B and continuing to the next crossroads.  We then compute the same for route B, but crossing on to path A instead.\n",
    "\n",
    "This gives us the ability to find the best routes for the next section of road on path A and path B.  We then choose the best route for A and B and append it to the existing path.  We add the new information to the start of the list rather than the end as it is computationally cheaper.  Effectively this makes the computed path run backwards; however, this can easily be reversed as a final step.  The new path to A and new path to B are returned as a pair.\n",
    "\n",
    "We can test the functionality by running the function on part of the route."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "([(C,30),(B,10)],[(B,10)])"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "roadStep ([], []) (head heathrowToLondon)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This list is reversed, so should be read right to left.  From this we can see that the best route to A is to start from B, cross over to A and then go directly forward.\n",
    "\n",
    "This function could be optimised further.  At the moment the price is calculated at each step; we could instead implement *roadStep* with the type signature \n",
    "```Haskell \n",
    "(Path, Path, Int, Int) -> Section -> (Path, Path, Int, Int)\n",
    "```\n",
    "to pass the best price on A and B each time.\n",
    "\n",
    "This leaves a functionm that takes a pair of paths and a section, producing a new optimal path.  We can do a left fold over this to form an optimal solution.  Initially the *roadStep* function is called with a pair of empty lists and the first section to return an optimal pair of paths over that section.  Those opitmal paths are called with the next section, until all the sections have been walked over.  The result is the shorter of the two optimal paths.  We can use this to write the *optimalPath* algorithm:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "optimalPath :: RoadSystem -> Path\n",
    "optimalPath roadSystem =\n",
    "    let (bestAPath, bestBPath) = foldl roadStep ([], []) roadSystem\n",
    "    in if sum (map snd bestAPath) <= sum (map snd bestBPath)\n",
    "        then reverse bestAPath\n",
    "        else reverse bestBPath"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We perform a left fold over *roadSystem* (a list of sections), starting with an inital accumulator of two empty paths.  This fold returns a pair of paths; hence, we pattern match the pair to return the individual paths.  The cheaper of the two is returned, having first been reversed.  Running the function gives the following output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[(B,10),(C,30),(A,5),(C,20),(B,2),(B,8),(C,0)]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "optimalPath heathrowToLondon"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is the expected result, apart from the *(C, 0)* operation at the end, which is a free step.  Now that the core functionality has been implemented we want to be able to read a road system from the standard input.  That can be converted to a *RoadSystem*, run through the algorithm, and then printed as a path.\n",
    "\n",
    "We begin by making a function to split a list into groups of the same size.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "groupsOf :: Int -> [a] -> [[a]]\n",
    "groupsOf 0 _ = undefined\n",
    "groupsOf _ [] = []\n",
    "groupsOf n xs = take n xs : groupsOf n (drop n xs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can test this with parameters of *3* and *[1..10]*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[[1,2,3],[4,5,6],[7,8,9],[10]]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "groupsOf 3 [1..10]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now combine that with out *optimalPath* algorithm.  First, we read contents from the standard input; we then call the *lines* function to convert the input into a list.  The resulting string list is mapped to the function *read* to convert into a list of numbers that can be split into groups of 3.  The lambda function ```(\\ [a, b, c] -> Section a b c)``` is used to take a list of 3 items to return a *Section*.  The data is now in a suitable form to be passed to the *optimalPath* function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<style>/* Styles used for the Hoogle display in the pager */\n",
       ".hoogle-doc {\n",
       "display: block;\n",
       "padding-bottom: 1.3em;\n",
       "padding-left: 0.4em;\n",
       "}\n",
       ".hoogle-code {\n",
       "display: block;\n",
       "font-family: monospace;\n",
       "white-space: pre;\n",
       "}\n",
       ".hoogle-text {\n",
       "display: block;\n",
       "}\n",
       ".hoogle-name {\n",
       "color: green;\n",
       "font-weight: bold;\n",
       "}\n",
       ".hoogle-head {\n",
       "font-weight: bold;\n",
       "}\n",
       ".hoogle-sub {\n",
       "display: block;\n",
       "margin-left: 0.4em;\n",
       "}\n",
       ".hoogle-package {\n",
       "font-weight: bold;\n",
       "font-style: italic;\n",
       "}\n",
       ".hoogle-module {\n",
       "font-weight: bold;\n",
       "}\n",
       ".hoogle-class {\n",
       "font-weight: bold;\n",
       "}\n",
       ".get-type {\n",
       "color: green;\n",
       "font-weight: bold;\n",
       "font-family: monospace;\n",
       "display: block;\n",
       "white-space: pre-wrap;\n",
       "}\n",
       ".show-type {\n",
       "color: green;\n",
       "font-weight: bold;\n",
       "font-family: monospace;\n",
       "margin-left: 1em;\n",
       "}\n",
       ".mono {\n",
       "font-family: monospace;\n",
       "display: block;\n",
       "}\n",
       ".err-msg {\n",
       "color: red;\n",
       "font-style: italic;\n",
       "font-family: monospace;\n",
       "white-space: pre;\n",
       "display: block;\n",
       "}\n",
       "#unshowable {\n",
       "color: red;\n",
       "font-weight: bold;\n",
       "}\n",
       ".err-msg.in.collapse {\n",
       "padding-top: 0.7em;\n",
       "}\n",
       ".highlight-code {\n",
       "white-space: pre;\n",
       "font-family: monospace;\n",
       "}\n",
       ".suggestion-warning { \n",
       "font-weight: bold;\n",
       "color: rgb(200, 130, 0);\n",
       "}\n",
       ".suggestion-error { \n",
       "font-weight: bold;\n",
       "color: red;\n",
       "}\n",
       ".suggestion-name {\n",
       "font-weight: bold;\n",
       "}\n",
       "</style><div class=\"suggestion-name\" style=\"clear:both;\">Use concatMap</div><div class=\"suggestion-row\" style=\"float: left;\"><div class=\"suggestion-error\">Found:</div><div class=\"highlight-code\" id=\"haskell\">concat $ map (show . fst) path</div></div><div class=\"suggestion-row\" style=\"float: left;\"><div class=\"suggestion-error\">Why Not:</div><div class=\"highlight-code\" id=\"haskell\">concatMap (show . fst) path</div></div>"
      ],
      "text/plain": [
       "Line 6: Use concatMap\n",
       "Found:\n",
       "concat $ map (show . fst) path\n",
       "Why not:\n",
       "concatMap (show . fst) path"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import Data.List  \n",
    "      \n",
    "main = do  \n",
    "    contents <- getContents  \n",
    "    let threes = groupsOf 3 (map read $ lines contents)  \n",
    "        roadSystem = map (\\[a,b,c] -> Section a b c) threes  \n",
    "        path = optimalPath roadSystem  \n",
    "        pathString = concat $ map (show . fst) path  \n",
    "        pathPrice = sum $ map snd path  \n",
    "    putStrLn $ \"The best path to take is: \" ++ pathString  \n",
    "    putStrLn $ \"The price is: \" ++ show pathPrice"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now test our entire function by running from the command line:\n",
    "\n",
    "```bash\n",
    "cat paths.txt | runhaskell heathrow.hs\n",
    "```\n",
    "\n",
    "Which gives the results:\n",
    "\n",
    "```\n",
    "The best path to take is: BCACBBC\n",
    "The price is: 75\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Haskell",
   "language": "haskell",
   "name": "haskell"
  },
  "language_info": {
   "codemirror_mode": "ihaskell",
   "file_extension": ".hs",
   "name": "haskell",
   "version": "7.10.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
