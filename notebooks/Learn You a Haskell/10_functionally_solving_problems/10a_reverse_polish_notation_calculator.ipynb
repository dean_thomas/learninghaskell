{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Reverse Polish notation calculator\n",
    "The traditional method used in schools for writing mathematical expressions is to write them in *infix notation*.  Whilst this is a simple form to be processed by humans, it requires the use of parentheses.\n",
    "\n",
    "Using *Reverse Polish Notation* the need for parentheses can be eliminated.    The basis of Reverse Polish Notation is a stack; the expression is evaluated left-to-right, when a number is encountered it is pushed onto the stack.  When an operator is encountered the two top numbers of the stack are popped, the operator applied to them, and the result pushed back onto the stack.  At the end of the calculation process the only number remaining on the stack should be the result."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "As an example we will evaluate the expression **10 4 3 + 2 * -**.  First **10** is pushed on to the stack, then **4**, then **3**; the stack is now **[ 10, 4, 3 ]**.  Next up is the **+** operator, the two numbers at the top of the stack (**4** and **3**) are popped from the stack, leaving it as just **[ 10 ]**.  We add **4** and **3**, giving us **7**; this is pushed onto the stack, making it **[ 10, 7 ]**.  Next in the expression we encounter **2**; this is pushed to the stack, giving us **[ 10, 7, 2 ]**.  Next we come across the **\\*** operator.  We pop **2** and **7** from the stack, multiply them together, and push the result **14** on to the stack.  Finally, we encounter the **-** operator, with the stack being **[ 10, 14 ]**.  **14** is subtracted from **10**, giving a result of **-4**, and pushed back on to the stack.  The expression has been fully evaluated and the only value left on the stack is **-4**, meaning this is the answer."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Having walked through and solved the problem, we can think about how to translate the problem to Haskell.  We will design the function to take an expression as a string and return the result as a number.  Therefore, we can expect the function to have a type of:\n",
    "\n",
    "```Haskell\n",
    "solveRPN :: (Num a) => String -> a\n",
    "```\n",
    "\n",
    "It is usually a good idea to think of the type declaration of the function prior to writing the implementation.  We can use our experience of solving the problem by hand to design our solution.  First up, we broke the string into a list of tokens separated by a space.\n",
    "\n",
    "This list was processed from left to right using a stack.  This is exactly the scenario where a *fold* can be used to process the input.  In this case we use a left fold and the accumulator is a stack.  We need to consider how to implement the stack; the head of the list can be used to represent the top of the stack, as adding to the head of a list is faster than the tail.\n",
    "\n",
    "We can now roughly formulate a solution.  We will take the string input, break it down into a list of items using **words**.  The list of words will be processed using a left fold, resulting in a stack of a single item.  The head of the single element list can be extracted and returned as the result.  An initial sketch of the function may be:\n",
    "\n",
    "```Haskell\n",
    "import Data.List\n",
    "\n",
    "solveRPN :: (Num a) => String -> a\n",
    "solveRPN expression = head (foldl foldingFunction [] (words expression))\n",
    "    where foldingFunction stack item = ...\n",
    "```\n",
    "\n",
    "In order to make the implementation easier to read, by reducing the number of brackets, we can rewrite it using point-free style.\n",
    "\n",
    "```Haskell\n",
    "import Data.List\n",
    "\n",
    "solveRPN :: (Num a) => String -> a\n",
    "solveRPN = head . foldl foldingFunction [] . words\n",
    "    where foldingFunction stack item ...\n",
    "```\n",
    "\n",
    "Finally, we can use pattern matching to implement the operators.  The patterns are tried from top to bottom, but if no match is found we will assume a number token.  We use **read** to get a number from the string, push the value to the stack and return the result.  The read type class allows the function to handle floating numbers along with integers.\n",
    "\n",
    "```Haskell\n",
    "import Data.List\n",
    "\n",
    "solveRPN :: (Num a, Read a) => String -> a\n",
    "solveRPN = head . foldl foldingFunction [] . words\n",
    "    where foldingFunction (x:y:ys) \"*\" = (x * y):ys\n",
    "          foldingFunction (x:y:ys) \"+\" = (x + y):ys\n",
    "          foldingFunction (x:y:ys) \"-\" = (x - y):ys\n",
    "          foldingFunction xs numberString = read numberString:xs\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Data.List\n",
    "\n",
    "solveRPN :: (Num a, Read a) => String -> a\n",
    "solveRPN = head . foldl foldingFunction [] . words\n",
    "    where foldingFunction (x:y:ys) \"*\" = (x * y):ys\n",
    "          foldingFunction (x:y:ys) \"+\" = (x + y):ys\n",
    "          foldingFunction (x:y:ys) \"-\" = (y - x):ys\n",
    "          foldingFunction xs numberString = read numberString:xs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "-4"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "solveRPN \"10 4 3 + 2 * -\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "5"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "solveRPN \"2 3 +\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "-3947"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "solveRPN \"90 34 12 33 55 66 + * - +\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4037"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "solveRPN \"90 34 12 33 55 66 + * - + -\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "87"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "solveRPN \"90 3 -\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A nice feature is that it is easy to expand the function with additional operators.  These operators don't need to mecessarily be binary operators; unary operators, such as *log* can pop a single number, other anity operators can also be implemented easily by pattern matching."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import Data.List  \n",
    "\n",
    "solveRPN :: String -> Float  \n",
    "solveRPN = head . foldl foldingFunction [] . words  \n",
    "    where   foldingFunction (x:y:ys) \"*\" = (x * y):ys  \n",
    "            foldingFunction (x:y:ys) \"+\" = (x + y):ys  \n",
    "            foldingFunction (x:y:ys) \"-\" = (y - x):ys  \n",
    "            foldingFunction (x:y:ys) \"/\" = (y / x):ys  \n",
    "            foldingFunction (x:y:ys) \"^\" = (y ** x):ys  \n",
    "            foldingFunction (x:xs) \"ln\" = log x:xs  \n",
    "            foldingFunction xs \"sum\" = [sum xs]  \n",
    "            foldingFunction xs numberString = read numberString:xs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we have implemented the unary *ln* operator and a *sum* operator that computes the sum of all values on the stack."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.9932518"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "solveRPN \"2.7 ln\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "10.0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "solveRPN \"10 10 10 10 sum 4 /\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "12.5"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "solveRPN \"10 10 10 10 10 sum 4 /\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "100.0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "solveRPN \"10 2 ^\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "6.575903"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "solveRPN \"43.2425 0.5 ^\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At present this function isn't very fault tolerant.  Giving the function input that can't be processed will crash it.  A fault tolerant version of the function is provided in later chapters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Haskell",
   "language": "haskell",
   "name": "haskell"
  },
  "language_info": {
   "codemirror_mode": "ihaskell",
   "file_extension": ".hs",
   "name": "haskell",
   "version": "7.10.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
