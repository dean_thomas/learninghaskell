
:t 'a'

:t True

:t "HELLO!"

:t (True, 'a')

:t 4 == 5

removeNonUppercase :: [Char] -> [Char]
removeNonUppercase st = [ c | c <- st, c `elem` ['A'..'Z']]

removeNonUppercase "HELLO there"

addThree :: Int -> Int -> Int -> Int
addThree x y z = x + y + z

addThree 1 2 3

:t addThree

factorial :: Integer -> Integer
factorial n = product [1..n]

factorial 50

circumference :: Float -> Float
circumference r = 2 * pi * r

circumference 4.0

circumference' :: Double -> Double
circumference' r = 2 * pi * r

circumference' 4.0

:t head

:t fst

:t (==)

:t (==)

:t (/=)

5 == 5

5 /= 5

'a' == 'a'

"Ho Ho" == "Ho Ho"

3.432 == 3.432

:t (>)

:t (<)

:t (<=)

:t (>=)

:t compare

"Abrakadabra" < "Zebra"

"Abrakadabra" `compare` "Zebra"

5 >= 2

5 `compare` 3

'b' > 'a'

:t show

show 3

show 5.334

show True

:t read

read "True" || False

read "8.2" + 3.8

read "5" - 2

read "[1, 2, 3, 4]" ++ [3]

read "4"

read "5" :: Int

read "5" :: Float

(read "5" :: Float) * 4

read "[1, 2, 3, 4]" :: [Int]

read "(3, 'a')" :: (Int, Char)

[read "True", False, True, False]

:t succ

:t pred

['a'..'e']

[LT .. GT]

[3 .. 5]

succ 'B'

pred 2.16

:t minBound

:t maxBound

(minBound :: Int)

(maxBound :: Char)

(maxBound :: Bool)

(minBound :: Bool)

(maxBound :: (Bool, Int, Char))

:t 20

20 :: Int

20 :: Integer

20 :: Float

20 :: Double

:t (*)

(5 :: Int) * (6 :: Integer)

:t fromIntegral

:t length

fromIntegral (length [1, 2, 3, 4]) + 3.2


