{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 5 Recursion"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recursion is a very important in Haskell as Haskell allows you to declare what something *is* instead of *how* it is implemented.  There are no loops in Haskell, instead we must achieve the behaviour using recursion.  Edge conditions are an important method of terminating recursive functions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Maximum awesome"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following function takes a list of orderable things (instances of the *Ord* typeclass) and returns the largest object.  In an imperative language this would probably be achieved using a loop and a variable to hold the maximum.  Here we will define it in Haskell in a recursive form.  First we setup an edge condition to terminate the function if the list only contains a single element.  We can then say that the maximum of a longer list is the head, if the head is bigger than the maximum of the tail."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "maximum' :: (Ord a) => [a] -> a\n",
    "maximum' [] = error \"maximum of empty list\"\n",
    "maximum' [x] = x\n",
    "maximum' (x:xs)\n",
    "    | x > maxTail = x\n",
    "    | otherwise = maxTail\n",
    "    where maxTail = maximum' xs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "500"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "maximum' [3, 4, 10, 500, 32]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pattern matching and recursion go very well together in Haskell.  The first two patterns simply define what to do in the two edge conditions; first, if the list is empty; second, if the list contains a single element; the actual recursion is defined in the third pattern.  This idiom is common when working with recursion and lists.  A *where* binding is used to define the maximum element in the rest of the list.  We can then check if the head is greater than the rest of the list, returning the head if it is or the maximum of the remaining list if not.  We could also define this function using the built-in **max** function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "maximum'' :: (Ord a) => [a] -> a\n",
    "maximum'' [] = error \"maximum of empty list\"\n",
    "maximum'' [x] = x\n",
    "maximum'' (x:xs) = max x (maximum'' xs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "445"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "maximum'' [3, 8, 20, 445, 4]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A few more recursive functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "### replicate\n",
    "The next function that we can implement using recursion is the **replicate** function.  This takes an int and some element and returns a list of repetitions of that element; *example:* **replicate 3 5** returns [5, 5, 5].  In order to implement the function recursively we need to define an edge condition.  The edge condition in this case is a repetition of zero (or less) elements - this should return an empty list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "replicate' :: (Num i, Ord i) => i -> a -> [a]\n",
    "replicate' n x\n",
    "    | n <= 0 = []\n",
    "    | otherwise = x:replicate' (n-1) x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[5,5,5]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "replicate' 3 5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It makes sense in this case to use guards instead of patterns, as we are testing a boolean condition.  If **n** is zero or less we return an empty list; otherwise, we return a list with **x** as the first element and a tail of **x** replicated **n-1** times.  Hence, we will eventually reach 0.  *Note:* **Num** is not a subclass of **Ord**, what constitutes as a number dosen't necessarily have an ordering.  In order to handle this, we specify **Num** and **Ord** as class constraints for the comparison."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### take\n",
    "The **take** function removes elements from a list.  *example:* **take** 3 [5, 4, 3, 2, 1] returns [5, 4, 3].  Edge conditions are attempting to take 0 or less items from a list or taking anything from an empty list, both edge cases return an empty list.  We can define the function as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "take' :: (Num i, Ord i) => i -> [a] -> [a]\n",
    "take' n _\n",
    "    | n <= 0   = []\n",
    "take' _ []     = []\n",
    "take' n (x:xs) = x : take' (n-1) xs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[5,4,3]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "take' 3 [5, 4, 3, 2, 1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first guard pattern protects against taking 0 or less elements from a list, *note:* we have no **otherwise** clause, meaning that if **n** is greater than zero we fall through to the next statement.  The second guard protects against taking elements from an empty list; as we don't actually care what the list contains we can match using **\\_**.  We then split the list into a head and tail in the third guard."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### reverse\n",
    "We can use **reverse** to reverse a list.  The edge condition here is the empty list (returning an empty list).  The rest of the list can be reversed by splitting in a head and a tail, reversing the tail recursively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "reverse' :: [a] -> [a]\n",
    "reverse' [] = []\n",
    "reverse' (x:xs) = reverse' xs ++ [x]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[5,4,3,2,1]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "reverse' [1, 2, 3, 4 ,5]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### repeat\n",
    "Haskell supports infinite lists; therefore, lists don't necessarily need an edge condition.  The good thing about infinite lists is that we can cut them where we like."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "repeat' :: a -> [a]\n",
    "repeat' x = x:repeat' x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[3,3,3,3,3]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "take' 5 (repeat' 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### zip\n",
    "We can use **zip** to merge two lists together.  If the two lists are different lengths, the longer list is truncated to the size of the shorter list.  Zipping with an empty list results in an empty list in return.  This is an edge condition; however, as we are dealing with two lists we actually need to define two edge conditions.  In the example below we handle these two edge conditions as two guards.  We then define the body of the function by pairing the head of the two lists and zipping the remaining tails."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "zip' :: [a] -> [b] -> [(a, b)]\n",
    "zip' _ [] = []\n",
    "zip' [] _ = []\n",
    "zip' (x:xs) (y:ys) = (x, y): zip' xs ys"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[(1,2),(2,3)]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "zip' [1, 2, 3] [2, 3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### elem\n",
    "We can use **elem** to test if an item is an element in a list.  Once again the edge condition is the empty list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "elem' :: (Eq a) => a -> [a] -> Bool\n",
    "elem' a [] = False\n",
    "elem' a (x:xs)\n",
    "    | a == x    = True\n",
    "    | otherwise = a `elem'` xs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "elem' 4 [1, 2, 3, 4, 5]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Quick, sort!\n",
    "In the next example we will implement the quick sort algorithm in Haskell.  In an imperative language it can be expected that writing the algorithm would take at least 10 lines, but in Haskell it is much simpler.  Quicksort is a well known example of the usage of Haskell for simply defining algoithms.\n",
    "\n",
    "Once again we will implement the algoithm as a recursive algoithm, with the standard edge condition of an empty list.  The main algorithm is as follows: a sorted list is a list that has all the values smaller than (or equal to) the head of the list in front (and those values are sorted), then comes the head of the list in the middle and then come all the values that are biggen than the head (they're also sorted)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "quicksort :: (Ord a) => [a] -> [a]\n",
    "quicksort [] = []\n",
    "quicksort (x:xs) =\n",
    "    let smaller_sorted = quicksort [a | a <- xs, a <= x]\n",
    "        bigger_sorted = quicksort [a | a <- xs, a > x]\n",
    "    in smaller_sorted ++ [x] ++ bigger_sorted"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[1,2,2,3,3,4,4,5,6,7,8,9,10]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "quicksort [10, 2, 5, 3, 1, 6, 7, 4, 2, 3, 4, 8, 9]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\"        abcdeeefghhijklmnoooopqrrsttuuvwxyz\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "quicksort \"the quick brown fox jumps over the lazy dog\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets analyse how the algorithm would sort the list [5, 1, 9, 4, 6, 7, 3].  First we take the head '5' abd put it in the middle of the two lists smaller and bigger than it.  We are now left with [1, 4, 3] ++ [5] ++ [9, 6, 7].  Now if we sort [1, 4, 3] and [9, 6, 7] we have a fully sorted list."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Thinking recursively\n",
    "There is a pattern to all of the examples of recursion defined here.  Usually, the first step is to define an edge case and then define a function to apply to the rest.  Recursion doesn't necessarily need to involve list structures; it can be used on trees or any other data structure.  When dealing with lists the edge case often involves an empty list, in tree structures the edge case is usually a node with no children.\n",
    "\n",
    "Dealing with numbers is a similar case.  Often a recursive function doesn't make sense for particular values, such as zero in a recursive factorial function.  We can often define the edge case as an identity; for multiplication the identity is 1, for addition the identity is 0.  Likewise, when sorting a list the identity becomes the empty list.  If you add an empty list to an empty list you return an empty list.\n",
    "\n",
    "Therefore, when trying to define a function recursively the best strategy is often to think of a situation where recursion doesn't work.  This can then be used as an edge case for the algorithm.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Haskell",
   "language": "haskell",
   "name": "haskell"
  },
  "language_info": {
   "codemirror_mode": "ihaskell",
   "file_extension": ".hs",
   "name": "haskell",
   "version": "7.10.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
