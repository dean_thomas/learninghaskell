--	lesson01.hs
--
--	Taken from: http://www.seas.upenn.edu/~cis194/lectures/01-intro.html
--	[29-07-2014]
--

{-	Needed for the getArgs command	--}
import System.Environment

{-	Entry point of the program	--}
main::IO()
main = do
	getArgs >>= print . hello . head
	print x
	print biggestInt
	print smallestInt
	print n
	print numDigits
	print d1
	print s1
	print b1
	print b2
	print c1
	print c2
	print c3
	print str1
	
{-	Read the following declaration as
	"x has type Int"	-}
x :: Int
x = 3

{-	Recursive?	-}
y :: Int
y = y + 1

{-	Find the largest and smallest natively
	sized integers on the system	-}
biggestInt, smallestInt :: Int
biggestInt = maxBound
smallestInt = minBound

{-	Nice: Arbitrary length integers (assign 2^200)	--}
n :: Integer
n = 1606938044258990275541962092341162602522202993782792835301376
numDigits :: Int
numDigits = length (show n)

{-	Floating point numbers	-}
d1 :: Double
d1 = 3.1415926535897932384626433832795028841971693993751058209
s1 :: Float
s1 = 3.1415926535897932384626433832795028841971693993751058209

{-	Boolean	-}
b1, b2 :: Bool
b1 = True
b2 = False

{-	Char's and strings	-}
c1, c2, c3 :: Char
c1 = 'A'
c2 = '5'
c3 = 'Я'
str1 :: String
str1 = "Hello world!"

{-	A function	-}
hello s = do
	"Hello! " ++ s


--print . x . 