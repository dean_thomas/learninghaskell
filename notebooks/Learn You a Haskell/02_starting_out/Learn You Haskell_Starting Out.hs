
2 + 15

49 * 100

1892 - 1472

5 /2 

(50 * 100) - 4999

50 * 100 - 4999

50 * (100 - 4999)

True && False

True && True

False || True

not False

not (True && True)

5 == 5

1 == 0

5 /= 5

5 /= 4

"hello" == "hello"

5 + "llama"

succ 8

min 9 10

min 3.4 3.2

max 100 101

succ 9 + max 5 4 + 1

(succ 9) + (max 5 4) + 1

92 `div` 10

div 92 10

doubleMe x = x + x

doubleMe 9

doubleMe 8.3

doubleUs x y = x * 2 + y * 2

doubleUs 4 9

doubleUs 2.3 34.2

doubleUs 28 88 + doubleMe 123

doubleUs x y = doubleMe x + doubleMe y

doubleUs 2.3 34.2

doubleSmallNumber x = if x > 100
                      then x
                      else x*2

doubleSmallNumber 5

doubleSmallNumber 50

doubleSmallNumber 101

doubleSmallNumber' x = (if x > 100 then x else x*2) + 1 

doubleSmallNumber'' x = if x > 100 then x else x*2 + 1

doubleSmallNumber' 150

doubleSmallNumber'' 150

conanO'Brien = "It's me, Conan O'Brien"

let lostNumbers = [4, 8, 15, 16, 23, 42]

lostNumbers

[1, 2, 3, 4] ++ [9, 10, 11, 12]

"hello" ++ " " ++ "world"

['w', 'o'] ++ ['o', 't']

'A' : " small cat"

5:[1, 2, 3, 4, 5]

[1, 2, 3, 4] ++ [5]

[1, 2, 3, 4] ++ 5

"Steve Buscemi" !! 6

[9.4, 33.2, 96.2, 11.2, 23.25] !! 1

[0, 1, 2, 3, 4] !! 6

let b = [[1, 2, 3, 4], [5, 3, 3, 3], [1, 2, 2, 3, 4], [1, 2, 3]]

b

b ++ [[1, 1, 1, 1]]

[6, 6, 6] : b

b !! 2

[3, 2, 1] > [2, 1, 0]

[3, 2, 1] > [2, 10, 100]

[3, 4, 2] < [3, 4, 3]

[3, 4, 2] > [2, 4]

[3, 4, 2] == [3, 4, 2]

[] > [1]

head [5, 4, 3, 2, 1]

tail [5, 4, 3, 2, 1]

last [5, 4, 3, 2, 1]

init [5, 4, 3, 2, 1]

head []

length [5, 4, 3, 2, 1]

null [1, 2, 3]

null []

reverse [5, 4, 3, 2, 1]

take 3 [1, 2, 3, 4, 5]

take 4 [1, 2, 3]

take 0 [5, 6, 7]

drop 3 [8, 4, 2, 1, 5, 6]

drop 0 [1, 2, 3, 4]

drop 100 [1, 2, 3, 4]

maximum [1, 9, 2, 3, 4]

minimum [8, 4, 2, 1, 5, 6]

sum [5, 2, 1, 6, 3, 2, 5, 7]

product [6, 2, 1, 2]

product [1, 2, 5, 6, 7, 9, 2, 0]

elem 4 [3, 4, 5, 6]

10 `elem` [3, 4, 5, 6]

[1..20]

['a'..'z']

['K'..'Z']

["Dan".."Dave"]

[2, 4..20]

[3, 6..20]

[20, 19..1]

[13, 26..24*13]

take 24 [13, 26..]

take 10 (cycle [1, 2, 3])

take 12 (cycle "LOL " )

take 10 (repeat 5)

replicate 3 10

[0.1, 0.3..1]

[x*2 | x <- [1..10]]

[x*2 | x <- [1..10], x*2 >= 12]

[x | x <- [50..100], x `mod` 7 == 3]

boomBangs xs = [ if x < 10 then "Boom!" else "Bang!" | x <- xs, odd x]

boomBangs[7..13]

boomBangs[1..20]

[ x | x <- [10..20], x /= 13, x /= 15, x /= 19]

[ x+y | x <- [1, 2, 3], y <- [10, 100, 1000]]

[ x*y | x <- [2, 5, 10], y <- [8, 10, 11], x*y > 50]

let nouns = ["hobo", "frog", "pope"]
let adjectives = ["lazy", "grouchy", "scheming"]
[adjective ++ " " ++ noun | adjective <- adjectives, noun <- nouns]

length' xs = sum [1 | _ <- xs]

length' [3, 4, 5, 10]

length' "hello world"

removeNonUppercase st = [ c | c <- st, c `elem` ['A'..'Z']]

removeNonUppercase "Hahaha! Ahahahaha!"

removeNonUppercase "IdontLIKEFROGS"

let xxs = [[1, 3, 5, 2, 3, 1, 2, 4, 5], [1, 2, 3, 4, 5, 6, 7, 8, 9], [1, 2, 4, 2, 1, 6, 3, 1, 3, 2, 3, 6]]

[[ x | x <- xs, even x ] | xs <- xxs]

(1, 3)

(3, 'a', "hello")

(50, 50.4, "hello", 'b')

[(1, 2), (8, 11, 5), (4, 5)]

fst (8, 11)

snd ("Wow", False)

zip [1, 2, 3, 4, 5] [5, 5, 5, 5, 5]

zip [1..5] ["one", "two", "three", "four", "five"]

zip [5, 3, 2, 6, 2, 7, 2, 5, 4, 6, 6] ["im", "a", "turtle"]

zip [1..] ["apple", "orange", "cherry", "mango"]

let triples = [ (a, b, c) | c <- [1..10], a <- [1..10], b <- [1..10]]

let rightTriangles = [ (a, b, c) | c <- [1..10], a <- [1..c], b <- [1..a], a^2 + b^2 == c^2]

rightTriangles

let rightTriangles' = [ (a, b, c) | c <- [1..10], a <- [1..c], b <- [1..a], a^2 + b^2 == c^2, a+b+c == 24]

rightTriangles'
